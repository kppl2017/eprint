<?php

class Userauthen extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('User_Model');
  }

  function index() { //Done
    $data['err_message'] = "";
    $this->load->view('userauthen/login', $data);
  }

  function login(){ //Done
    $email = $this->input->post('email');
    $password = $this->input->post('password');

    $this->load->library('form_validation');
    $this->form_validation->set_rules('password', 'Password', 'required');
    $this->form_validation->set_rules('email', 'Email', 'required');

    if ($this->form_validation->run() == FALSE)
    {
      $data['err_message'] = "Masukkan data dengan lengkap";
      $this->load->view('userauthen/login', $data);
    } else {
      $password = sha1(md5($password));
      $isLogin = $this->User_Model->login($email, $password);
      $i = $this->User_Model->authen_user($email);

      if($isLogin == true){
        $user_session = array(
          'surename' => $i[0]['surename'],
          'id_number' => $i[0]['id_number'],
          'emoney' => $i[0]['emoney'] );
          $this->session->set_userdata($user_session);
          redirect('home'); } else {
            $data['err_message'] = "User tidak ada atau Password salah";
            $this->load->view('userauthen/login', $data);
          }
        }
      }

      function logout(){ //Done
        $this->session->unset_userdata('surename');
        $this->session->unset_userdata('id_number');
        $this->session->unset_userdata('emoney');
        $this->session->sess_destroy();
        redirect('/');}

        function loadsignup(){ //Done
          $data['err_message'] = "";
          $this->load->view('userauthen/signup', $data);
        }

        function signup(){
          $id_number = $this->input->post('id_number');
          $email = $this->input->post('email');
          $surename = $this->input->post('surename');
          $password = $this->input->post('password');
          $repassword = $this->input->post('repassword');

          $this->load->library('form_validation');
          $this->form_validation->set_rules('id_number', 'ID Number', 'required');
          $this->form_validation->set_rules('email', 'Email', 'required');
          $this->form_validation->set_rules('surename', 'Surename', 'required');
          $this->form_validation->set_rules('password', 'Password', 'required');
          $this->form_validation->set_rules('repassword', 'Re-Password', 'required');

          if ($this->form_validation->run() == FALSE)
          {
            $data['err_message'] = "Masukkan data dengan lengkap";
            $this->load->view('userauthen/signup', $data);
          } else {
            if(strcmp($password,$repassword)==0){
              $isLogin = $this->User_Model->check_id($id_number);
              if($isLogin == true){
                $password = sha1(md5($password));
                $data = array(
                  'id_number' => $id_number,
                  'email' => $email,
                  'surename' => $surename,
                  'password' => $password
                );

                $status = $this->User_Model->addCustomer($data);
                if($status==1){
                  redirect(base_url());} else {
                    $data['err_message'] = "Registrasi gagal";
                    $this->load->view('userauthen/signup', $data);
                  }
                } else {
                  $data['err_message'] = "Id Number sudah digunakan";
                  $this->load->view('userauthen/signup', $data);
                }
              } else {
                $data['err_message'] = "Password tidak sama";
                $this->load->view('userauthen/signup', $data);
              }
            }
          }

          function viewdata($id_number){ //Done
            $info['data'] = $this->User_Model->getProfile($id_number);
            if($info['data']==null){
              echo "Forbidden Access";
            } else {
              $this->load->view('userauthen/custprofile', $info);
            }
          }

          function loadedit($id_number){
            $info['data'] = $this->User_Model->getProfile($id_number);
            if($info['data'][0]['id_number'] == $this->session->userdata('id_number')){
              $info['err_message'] = null;
              $this->load->view('userauthen/editprofile', $info);
            } else {
              echo "Forbidden Access";
            }
          }

          function editprofile(){
            $id_number = $this->input->post('id_number');
            $email = $this->input->post('email');
            $surename = $this->input->post('surename');
            $password = $this->input->post('password');
            $repassword = $this->input->post('re_password');

            $this->load->library('form_validation');
            $this->form_validation->set_rules('id_number', 'ID Number', 'required');
            $this->form_validation->set_rules('email', 'Email', 'required');
            $this->form_validation->set_rules('surename', 'Surename', 'required');
            $this->form_validation->set_rules('password', 'Password', 'required');
            $this->form_validation->set_rules('re_password', 'Re-Password', 'required');

            if ($this->form_validation->run() == FALSE)
            {
              $info['data'] = $this->User_Model->getProfile($id_number);
              $info['err_message'] = "Data harus terisi";
              $this->load->view('userauthen/editprofile', $info);
            } else {
              $password = sha1(md5($password));
              $repassword = sha1(md5($repassword));
              $data = array(
                'id_number' => $id_number,
                'email' => $email,
                'surename' => $surename,
                'password' => $password
              );
              if(strcmp($password,$repassword)==0){
                $status = $this->User_Model->editProfile($id_number, $data);
                $info['data'] = $this->User_Model->getProfile($id_number);
                if($status==1){
                  $info['err_message'] = "Data berhasil di update";
                } else {
                  $info['err_message'] = "Data gagal di update";
                }
                $this->load->view('userauthen/editprofile', $info);
              } else {
                $info['data'] = $this->User_Model->getProfile($id_number);
                $info['err_message'] = "Password tidak sama";
                $this->load->view('userauthen/editprofile', $info);
              }
            }
          }
        }

        ?>
