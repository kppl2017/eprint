<?php

class Viewloader extends CI_Controller {
  public function __construct() {
    parent::__construct();
    $this->load->model('User_Model');
  }

  function index() {
    $this->load->view('mainpagecust/home');
  }

  function layanan(){
    $this->load->view('mainpagecust/layanan');
  }

  function jasaPrint(){
    $this->load->view('mainpagecust/daftarprint');
  }

  function statusPemesanan(){
    $this->load->view('mainpagecust/statuspesan');
  }

  function contactUs(){
    $this->load->view('mainpagecust/contact');
  }
}

?>
