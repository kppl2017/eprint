<?php

class User_Model extends CI_Model {
  function login($email, $password){
    $this->db->select('*');
    $this->db->where('email', $email);
    $this->db->where('password', $password);
    $this->db->from('customer');

    $query = $this->db->get();
    if ($query->num_rows() == 1) {
      # code...
      return true;
    }
    else{
      return false;
    }
  }

  function authen_user($email){
    $this->db->select('*');
    $this->db->where('email', $email);
    $this->db->from('customer');
    $query = $this->db->get();
    return $query->result_array();
  }

  function getProfile($id_number){
    $this->db->select('*');
    $this->db->where('id_number', $id_number);
    $this->db->from('customer');
    $query = $this->db->get();
    return $query->result_array();
  }

  function check_id($id_number){
    $this->db->select('id_number');
    $this->db->where('id_number', $id_number);
    $this->db->from('customer');
    $query = $this->db->get();
    if ($query->num_rows() == 1) {
      return false;
    }
    else{
      return true;
    }
  }

  function addCustomer($data){
    $status = $this->db->insert('customer', $data);
    return $status;
  }

  function editProfile($id_number, $data){
    $this->db->where('id_number', $id_number);
    $status = $this->db->update('customer', $data);
    return $status;
  }
}

?>
