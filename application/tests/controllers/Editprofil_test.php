<?php


class Editprofile_test extends TestCase
{
	public function setUp()
	{
		$this->resetInstance();
	}



	public function test_editprofil_benar(){
		$_SESSION['emoney']='0';
		$_SESSION['id_number'] = '1234567899';
		$_SESSION['surename'] = 'Johny English';
		$result = $this->request('POST', 'Userauthen/editprofile',
		[
			'id_number' => '1234567899',
			'email' => 'test2@gmail.com',
			'surename' => 'Johny English',
			'password' => 'test1',
			're_password' => 'test1',

		]);

		$this->assertContains('Data berhasil di update', $result);


	}


	public function test_editprofil_salah1(){
		$_SESSION['emoney']='0';
		$_SESSION['id_number'] = '1234567899';
		$_SESSION['surename'] = 'Johny English';
		$result = $this->request('POST', 'Userauthen/editprofile',
		[
			'id_number' => '1234567899',
			'email' => 'test2@gmail.com',
			'surename' => 'Johny English',
			'password' => 'test1',
			're_password' => 'test2',

		]);

		$this->assertContains('Password tidak sama', $result);


	}

	public function test_editprofil_salah2(){
		$_SESSION['emoney']='0';
		$_SESSION['id_number'] = '1234567899';
		$_SESSION['surename'] = 'Johny English';
		$result = $this->request('POST', 'Userauthen/editprofile',
		[
			'id_number' => '1234567899',
			'email' => '',
			'surename' => 'Johny English',
			'password' => 'test1',
			're_password' => 'test1',

		]);

		$this->assertContains('Data harus terisi', $result);


	}

	public function test_editprofil_belomlogin(){
		$result = $this->request('POST', 'Userauthen/editprofile',
		[
			'id_number' => '1234567899',
			'email' => 'test2@gmail.com',
			'surename' => 'Johny English',
			'password' => 'test1',
			're_password' => 'test1',

		]);

		$this->assertRedirect('/');


	}


}

?>
