<?php

class Loadedit_test extends TestCase
{
  public function setUp()
  {
    $this->resetInstance();
  }

  public function test_loadedit_benar(){
    $_SESSION['id_number'] = '1234567890';
    $_SESSION['surename'] = 'Faisal Wilmar';
    $output = $this->request('GET', 'editprofile/1234567890');
    $this->assertContains('Faisal Wilmar', $output);
  }

  public function test_loadedit_belumlogin(){
    $_SESSION['id_number'] = '11234567890';
    $output = $this->request('GET', 'editprofile/1234567899');
    $this->assertContains('Forbidden Access', $output);
  }
}

?>
