<?php

class Login_test extends TestCase
{
  public function setUp()
  {
    $this->resetInstance();
  }

  public function test_login_salah1()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => 'faisalwilmar@gmail.com',
      'password' => '',
    ]);
    $this->assertContains('Masukkan data dengan lengkap', $result);
    $this->assertFalse(isset($_SESSION['surename']) );
  }

  public function test_login_benar()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => 'faisalwilmar@gmail.com',
      'password' => 'faisal',
    ]);
    $this->assertTrue(isset($_SESSION['surename']) );
    $this->assertRedirect('home');
  }

  public function test_login_salah2()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => '',
      'password' => 'faisal',
    ]);
    $this->assertContains('Masukkan data dengan lengkap', $result);
    $this->assertFalse( isset($_SESSION['surename']) );
  }

  public function test_login_salah3()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => '',
      'password' => '',
    ]);
    $this->assertContains('Masukkan data dengan lengkap', $result);
    $this->assertFalse(isset($_SESSION['surename']) );
  }

  public function test_login_salah4()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => 'faisalwil@gmail.com',
      'password' => 'faisal1',
    ]);
    $this->assertContains('User tidak ada atau Password salah', $result);
    $this->assertFalse( isset($_SESSION['surename']) );
  }

  public function test_login_sqli()
  {
    $result = $this->request('POST', 'Userauthen/login',
    [
      'email' => "'or'1'='1'",
      'password' => 'faisal1',
    ]);
    $this->assertContains('User tidak ada atau Password salah', $result);
    $this->assertFalse( isset($_SESSION['surename']) );
  }
}

?>
