<?php


class Logout_test extends TestCase
{
	public function setUp()
    {
        $this->resetInstance();
    }

	public function test_logout(){
		$_SESSION['surename'] = "Faisal Wilmar";
		$this->request('GET', 'Userauthen/logout');
		$this->assertFalse(isset($_SESSION['surename']));
		$this->assertRedirect('/');}
}
?>
