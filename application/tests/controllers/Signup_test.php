<?php


class Signup_test extends TestCase
{
	public function setUp()
    {
        $this->resetInstance();
    }

	
	
	public function test_signup_benar(){
		  $result = $this->request('POST', 'Userauthen/signup',
    [
	  'id_number' => '567286736',
      'email' => 'test2@gmail.com',
	  'surename' => 'test1',
      'password' => 'test1',
	  'repassword' => 'test1',
	  
    ]);
	
	
    
    $this->assertRedirect(base_url());
}


	public function test_signup_salah1(){
		  $result = $this->request('POST', 'Userauthen/signup',
    [
	  'id_number' => '1234567890',
      'email' => 'test2@gmail.com',
	  'surename' => 'test1',
      'password' => 'test1',
	  'repassword' => 'test1',
	  
    ]);
	
	
    
   $this->assertContains('Id Number sudah digunakan', $result);
	}
	
	public function test_signup_salah2(){
		  $result = $this->request('POST', 'Userauthen/signup',
    [
	  'id_number' => '8269826986',
      'email' => 'test2@gmail.com',
	  'surename' => 'test1',
      'password' => 'test1',
	  'repassword' => 'test12',
	  
    ]);
	
	
  
   $this->assertContains('Password tidak sama', $result);
	}
	
	public function test_signup_salah3(){
		  $result = $this->request('POST', 'Userauthen/signup',
    [
	  'id_number' => '1234567890',
      'email' => '',
	  'surename' => 'test1',
      'password' => 'test1',
	  'repassword' => 'test1',
	  
    ]);
	
	
    
   $this->assertContains('Masukkan data dengan lengkap', $result);
	}
	
	public function test_signup_udahlogin(){
		$_SESSION['surename'] = 'Johny';
		  $result = $this->request('POST', 'Userauthen/signup',
    [
	  'id_number' => '567286736',
      'email' => 'test2@gmail.com',
	  'surename' => 'test1',
      'password' => 'test1',
	  'repassword' => 'test1',
	  
    ]);
	
    $this->assertRedirect('home');
}
}
?>