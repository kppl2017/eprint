<?php
class View_test extends TestCase
{
  public function setUp()
  {
    $this->resetInstance();
  }

  public function test_view_home(){
    $_SESSION['id_number']='1234567890';
    $output = $this->request('GET', 'viewloader');
    $this->assertContains('<title>Homepage</title>', $output);
  }

  public function test_view_home_fail(){
    $output = $this->request('GET', 'viewloader');
    $this->assertRedirect('/');
  }

  public function test_view_layanan(){
    $_SESSION['id_number']='1234567890';
    $output = $this->request('GET', 'viewloader/layanan');
    $this->assertContains('<title>Layanan</title>', $output);
  }

  public function test_view_layanan_fail(){
    $output = $this->request('GET', 'viewloader/layanan');
    $this->assertRedirect('/');
  }

  public function test_view_jasaprint(){
    $_SESSION['id_number']='1234567890';
    $output = $this->request('GET', 'viewloader/jasaPrint');
    $this->assertContains('<title>Penyedia Jasa</title>', $output);
  }

  public function test_view_jasaprint_fail(){
    $output = $this->request('GET', 'viewloader/jasaPrint');
    $this->assertRedirect('/');
  }

  public function test_view_statuspemesanan(){
    $_SESSION['id_number']='1234567890';
    $output = $this->request('GET', 'viewloader/statusPemesanan');
    $this->assertContains('<title>Status Pesanan</title>', $output);
  }

  public function test_view_statuspemesanan_fail(){
    $output = $this->request('GET', 'viewloader/statusPemesanan');
    $this->assertRedirect('/');
  }

  public function test_view_contact(){
    $_SESSION['id_number']='1234567890';
    $output = $this->request('GET', 'viewloader/contactUs');
    $this->assertContains('<title>Contact</title>', $output);
  }

  public function test_view_contact_fail(){
    $output = $this->request('GET', 'viewloader/contactUs');
    $this->assertRedirect('/');
  }

  public function test_view_login(){
    $output = $this->request('GET', '/');
    $this->assertContains('<title>Login</title>', $output);
  }

  public function test_view_alraady_login(){
    $_SESSION['surename']='Faisal Wilmar';
    $output = $this->request('GET', '/');
    $this->assertRedirect('home');
  }
}

?>
