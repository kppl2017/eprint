<?php

class Viewdata_test extends TestCase
{
  public function setUp()
  {
    $this->resetInstance();
  }

  public function test_viewdata_active(){
    $_SESSION['surename'] = 'Faisal Wilmar';
    $output = $this->request('GET', 'profile/1234567890');
    $this->assertContains('Faisal Wilmar', $output);
  }

  public function test_viewdata_active_notlogin(){
    $output = $this->request('GET', 'profile/1234567890');
    $this->assertRedirect('/');
  }

  public function test_viewdata_suspended(){
    $_SESSION['surename'] = 'Faisal Wilmar';
    $output = $this->request('GET', 'profile/1234567899');
    $this->assertContains('Johny English', $output);
  }

  public function test_viewdata_suspended_notlogin(){
    $output = $this->request('GET', 'profile/1234567899');
    $this->assertRedirect('/');
  }

  public function test_viewdata_unavailable(){
    $_SESSION['surename'] = 'Faisal Wilmar';
    $output = $this->request('GET', 'profile/123');
    $this->assertContains('Forbidden Access', $output);
  }
}

?>
