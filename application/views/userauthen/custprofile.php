<?php
if(null!==$this->session->userdata('surename')){
} else {
	redirect('/');
}
?>
<html>
<head>
<title>Profile</title>
<link rel="stylesheet" href="<?php echo base_url().'assets/css/materialize.min.css';?>">
</head>
<body>
	<nav style="background-color:#424242">
		<div class="nav-wrapper">
			<ul id="nav-mobile" class="left hide-on-med-and-down">
				<li><a href="<?php echo base_url().'home'; ?>">Home</a></li>
				<li><a href="<?php echo base_url().'layanan'; ?>">Layanan</a></li>
				<li><a href="<?php echo base_url().'jasaprint'; ?>">Jasa Print</a></li>
				<li><a href="<?php echo base_url().'statuspemesanan'; ?>">Status Pemesanan</a></li>
				<li><a href="<?php echo base_url().'contact'; ?>">Contact Us</a></li>
			</ul>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="<?php echo base_url().'index.php/profile/'.$_SESSION['id_number']; ?>"><?php echo $_SESSION['surename'];?></a></li>
				<li><a href="#">Rp. <?php echo $_SESSION['emoney'];?></a></li>
				<li><a href="<?php echo base_url().'index.php/Userauthen/logout' ?>">Logout</a></li>
			</ul>
		</div>
	</nav>
<div class="container">
<p class="flow-text">
	<?php
	foreach ($data as $key) {
		echo $key['id_number']."<br>";
		echo $key['surename']."<br>";
		echo $key['email']."<br>";
		echo $key['emoney']."<br>";
		echo $key['phone']."<br>";
		if($key['status']==1){
			echo "Active";
		} else {
			echo "Suspended";
		}
	?>
<br/>
<a href="<?php echo base_url().'index.php/editprofile/'.$key['id_number'];?>">Edit Profile</a>
</div></p>

<?php } ?>
<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js';?>"></script>
<script src="<?php echo base_url().'assets/js/materialize.min.js';?>"></script>
</body>
</html>
