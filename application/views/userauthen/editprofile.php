<!DOCTYPE html>
<?php
if(null!==$this->session->userdata('surename')){
} else {
	redirect('/');
}
?>
<html>
<head>
	<title>Edit Profile</title>
	<link rel="stylesheet" href="<?php echo base_url().'assets/css/materialize.min.css';?>">
</head>
<body onload="Materialize.toast('<?php echo $err_message; ?>', 4000)">

	<nav style="background-color:#424242">
		<div class="nav-wrapper">
			<ul id="nav-mobile" class="left hide-on-med-and-down">
				<li><a href="<?php echo base_url().'home'; ?>">Home</a></li>
				<li><a href="<?php echo base_url().'layanan'; ?>">Layanan</a></li>
				<li><a href="<?php echo base_url().'jasaprint'; ?>">Jasa Print</a></li>
				<li><a href="<?php echo base_url().'statuspemesanan'; ?>">Status Pemesanan</a></li>
				<li><a href="<?php echo base_url().'contact'; ?>">Contact Us</a></li>
			</ul>
			<ul id="nav-mobile" class="right hide-on-med-and-down">
				<li><a href="<?php echo base_url().'index.php/profile/'.$_SESSION['id_number']; ?>"><?php echo $_SESSION['surename'];?></a></li>
				<li><a href="#">Rp. <?php echo $_SESSION['emoney'];?></a></li>
				<li><a href="<?php echo base_url().'index.php/Userauthen/logout' ?>">Logout</a></li>
			</ul>
		</div>
	</nav>

	<div class="container">
	<div class="row" style="margin-top: 50px">
		<?php foreach ($data as $key) { ?>
			<form class="col s12" method="post" action="<?php echo base_url().'index.php/Userauthen/editprofile'?>">
				<div class="row">
					<div class="input-field col s6">
						<input id="id_number" type="text" class="validate" name="id_number" value="<?php echo $key['id_number'];?>" readonly="readonly">
						<label for="first_name">ID Number</label>
					</div>
					<div class="input-field col s6">
						<input id="surename" type="text" class="validate" name="surename" value="<?php echo $key['surename'];?>">
						<label for="last_name">Real Name</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="email" type="email" class="validate" name="email" value="<?php echo $key['email'];?>">
						<label for="email">Email</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="password" value="">
						<label for="password">Password</label>
					</div>
				</div>
				<div class="row">
					<div class="input-field col s12">
						<input id="password" type="password" class="validate" name="re_password" value="">
						<label for="password">Re-Type Password</label>
					</div>
				</div>
				<button class="waves-effect waves-light btn" type="submit" value="UPDATE" style="background-color:#9e9e9e">UPDATE</button>
			</form>
			<?php } ?>
		</div>
	</div>



	<script type="text/javascript" src="<?php echo base_url().'assets/js/jquery.min.js';?>"></script>
	<script src="<?php echo base_url().'assets/js/materialize.min.js';?>"></script>
</body>
</html>
