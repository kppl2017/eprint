<!DOCTYPE html>
<?php
if(null!==$this->session->userdata('surename')){
  redirect('home');} else {
}
?>
<html>
<head>
<title>Sign Up</title>
  <link href="https://fonts.googleapis.com/icon?family=Material+Icons" rel="stylesheet">
  <link rel="stylesheet" type="text/css" href="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/css/materialize.min.css">
  <style>
    body {
      display: flex;
      min-height: 100vh;
      flex-direction: column;
    }

    main {
      flex: 1 0 auto;
    }

    body {
      background: #fff;
    }

    .input-field input[type=date]:focus + label,
    .input-field input[type=text]:focus + label,
    .input-field input[type=email]:focus + label,
    .input-field input[type=password]:focus + label {
      color: #e91e63;
    }

    .input-field input[type=date]:focus,
    .input-field input[type=text]:focus,
    .input-field input[type=email]:focus,
    .input-field input[type=password]:focus {
      border-bottom: 2px solid #e91e63;
      box-shadow: none;
    }
  </style>
</head>

<body>
  <div class="section"></div>
  <main>
    <center>

      <div class="section"></div>

      <h5 class="indigo-text">Register</h5>
      <div class="section"></div>

      <div class="container">
        <div class="z-depth-1 grey lighten-4 row" style="display: inline-block; padding: 32px 48px 0px 48px; border: 1px solid #EEE;">

          <form class="col s12" method="post" action="<?php echo base_url().'Userauthen/signup';?>">
            <div class='row'>
              <div class='col s12'>
              </div>
            </div>

						<div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='text' name='id_number' id='id_number' />
                <label for='id_number'>Enter your ID Number (No.KTP)</label>
              </div>
            </div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='email' name='email' id='email' />
                <label for='email'>Enter your email</label>
              </div>
            </div>

						<div class='row'>
							<div class='input-field col s12'>
								<input class='validate' type='text' name='surename' id='surename' />
								<label for='surename'>Enter your Real Name</label>
							</div>
						</div>

            <div class='row'>
              <div class='input-field col s12'>
                <input class='validate' type='password' name='password' id='password' />
                <label for='password'>Enter your password</label>
              </div>
							<div class='input-field col s12'>
								<input class='validate' type='password' name='repassword' id='repassword' />
								<label for='repassword'>Re-type your password</label>
							</div>
              <label style='float: right;'>
								<a class='pink-text' href='#!'><b><?php echo $err_message;?></b></a>
							</label>
            </div>

            <br />
            <center>
              <div class='row'>
                <button type='submit' name='btn_login' class='col s12 btn btn-large waves-effect indigo' value="LOGIN">Sign Up</button>
              </div>
            </center>
          </form>
        </div>
      </div>
      <a href="<?php echo base_url();?>">Already have account</a>
    </center>

    <div class="section"></div>
    <div class="section"></div>
  </main>

  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.2.1/jquery.min.js"></script>
  <script type="text/javascript" src="https://cdnjs.cloudflare.com/ajax/libs/materialize/0.97.5/js/materialize.min.js"></script>
</body>

</html>
