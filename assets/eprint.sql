-- phpMyAdmin SQL Dump
-- version 4.6.5.2
-- https://www.phpmyadmin.net/
--
-- Host: localhost
-- Generation Time: Oct 01, 2017 at 10:41 AM
-- Server version: 10.1.21-MariaDB
-- PHP Version: 7.1.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Database: `eprint`
--

-- --------------------------------------------------------

--
-- Table structure for table `customer`
--

CREATE TABLE `customer` (
  `id_number` varchar(25) NOT NULL,
  `email` varchar(100) NOT NULL,
  `surename` varchar(255) NOT NULL,
  `password` varchar(50) NOT NULL,
  `emoney` int(11) NOT NULL,
  `phone` varchar(15) NOT NULL DEFAULT 'NA',
  `status` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 ROW_FORMAT=COMPACT;

--
-- Dumping data for table `customer`
--

INSERT INTO `customer` (`id_number`, `email`, `surename`, `password`, `emoney`, `phone`, `status`) VALUES
('123123123', 'fafawilmar@gmail.com', 'Faisal Johny', 'wilmar', 0, 'NA', 0),
('1234567890', 'faisalwilmar@gmail.com', 'Faisal Wilmar', 'faisal', 20000, '81232249778', 1),
('1234567899', 'fafawilmar@gmail.com', 'Johny English', 'wilmar', 0, 'NA', 0),
('9087654321', 'faisalwilmar@yahoo.com', 'Faisal Willy', 'qwerty', 0, 'NA', 0);

--
-- Indexes for dumped tables
--

--
-- Indexes for table `customer`
--
ALTER TABLE `customer`
  ADD PRIMARY KEY (`id_number`);

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
